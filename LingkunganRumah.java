import java.awt.Color;
import java.lang.*;
public class LingkunganRumah{
	public static void main(String [] args){
		// membuat objek
		Kucing Michael = new Kucing();
		Kucing Garfield = new Kucing();
		// mengisi nama
		Michael.setName("Michael");
		Garfield.setName("Garfield");
		// mengisi umur 
		Michael.setUsia(3);
		Garfield.setUsia(5);
		//mengisi berat badan
		Michael.setBb(2.5);
		Garfield.setBb(6.8);
		//mengisi majikan
		Michael.setMajikan("Rezki");
		Garfield.setMajikan("Zaki");
		//cek status jinak
		Michael.getStatusJinak();
		Garfield.getStatusJinak();
		//Cetak info
		System.out.println("Nama Kucing: "+Michael.getName());
		System.out.println("Usia Kucing: "+Michael.getUsia());
		System.out.println("Berat badan Kucing: "+Michael.getBb());
		System.out.println("Nama majikan Kucing: "+Michael.getMajikan());
		System.out.println("Apakah Jinak?: "+Michael.getStatusJinak());
		System.out.println();
		//Cetak info kucing ke - 2
		System.out.println("Nama Kucing: "+Garfield.getName());
		System.out.println("Usia Kucing: "+Garfield.getUsia());
		System.out.println("Berat badan Kucing: "+Garfield.getBb());
		System.out.println("Nama majikan Kucing: "+Garfield.getMajikan());
		System.out.println("Apakah Jinak?: "+Garfield.getStatusJinak());
	}
}