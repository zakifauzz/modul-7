public class StudentRecord{
	private String name;
	private String address;
	private int age ;
	private double mathGrade;
	private double englishGrade;
	private double scienceGrade;
	private double average;
	private static int studentCount;
	
	public String getName(){
		return name;
	}
	public void setName(String temp){
		name=temp;
	}
	public String getAddress(){
		return address;
	}
	public void setAddress(String temp){
		address=temp;
	}
	public int getAge(){
		return age;
	}
	public void setAge(int temp){
		age=temp;
	}
	public double getMathGrade(){
		return mathGrade;
	}
	public void setMathGrade(double temp){
		mathGrade=temp;
	}
	public double getEnglishGrade(){
		return englishGrade;
	}
	public void setEnglishGrade(double temp){
		englishGrade=temp;
	}
	public double getScienceGrade(){
		return scienceGrade;
	}
	public void setScienceGrade(double temp){
		scienceGrade=temp;
	}
	public double getAverage(){
		double result = 0;
		result = (mathGrade+englishGrade+scienceGrade)/3;
		return result;
	}
	public void setAverage(double temp1, double temp2, double temp3){
		mathGrade=temp1;
		englishGrade=temp2;
		scienceGrade=temp3;
	}	
	public static int getStudentCount(){
		return studentCount;
	}
	public void setStudentCount(int temp){
		studentCount=temp;
	}
}