import java.awt.Color;
public class Horse{
	private String nama;
	private String warnaBulu;
	private int usia;
	private double bb;
	private int tinggi;

	public void cetakInformasi(){
		System.out.println("Horse");
		System.out.println("Nama         :"+nama);
		System.out.println("Warna Bulu   :"+warnaBulu);
		System.out.println("Usia         :"+usia);
		System.out.println("Berat Badan  :"+bb);
		System.out.println("Tinggi Badan :"+tinggi+"cm");
	}
	public void setname(String tmp){
		nama=tmp;
	}
	public String getname(){
		return nama;
	}
	public void setcolor(String tmp){
		warnaBulu=tmp;
	}
	public String getcolor(){
		return warnaBulu;
	}
	public void setusia(int tmp){
		usia=tmp;
	}
	public int getusia(){
		return usia;
	}
	public void setbb(double tmp){
		bb=tmp;
	}
	public double getbb(){
		return bb;
	}
	public void settinggi(int tmp){
		tinggi=tmp;
	}
	public int gettinggi(){
		return tinggi;
	}	
}
