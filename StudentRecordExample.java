public class StudentRecordExample{
	public static void main(String[]args){
		StudentRecord annaRecord = new StudentRecord();
		StudentRecord beahRecord = new StudentRecord();
		StudentRecord crisRecord = new StudentRecord();
		
		int count1=1;
	
		annaRecord.setName("Anna");
		annaRecord.setStudentCount(count1++);
		annaRecord.setMathGrade(80.0);
		annaRecord.setEnglishGrade(80.5);
		annaRecord.setScienceGrade(60.5);
		annaRecord.setAverage(annaRecord.getMathGrade(),annaRecord.getEnglishGrade(),annaRecord.getScienceGrade());
		System.out.println(annaRecord.getName());
		System.out.println("average= "+annaRecord.getAverage());
	
		beahRecord.setName("Beah");
		beahRecord.setStudentCount(count1++);
		beahRecord.setMathGrade(80);
		beahRecord.setEnglishGrade(70);
		beahRecord.setScienceGrade(75.5);
		beahRecord.setAverage(beahRecord.getMathGrade(),beahRecord.getEnglishGrade(),beahRecord.getScienceGrade());
		System.out.println(beahRecord.getName());
		System.out.println("average= "+ beahRecord.getAverage());
	
		crisRecord.setName("Cris");
		crisRecord.setStudentCount(count1++);
		crisRecord.setMathGrade(60);
		crisRecord.setEnglishGrade(90);
		crisRecord.setScienceGrade(65);
		crisRecord.setAverage(crisRecord.getMathGrade(),crisRecord.getEnglishGrade(),crisRecord.getScienceGrade());
		System.out.println(crisRecord.getName());
		System.out.println("average= "+crisRecord.getAverage());
	
		System.out.println("Count= "+StudentRecord.getStudentCount());
	}
}