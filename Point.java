public class Point{
	private int x;
	private int y;
	private int z;
	
	// Konstruktor
	public Point(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	//geser titik pada ruang
	public void geser(int dx, int dy, int dz){
		x+=dx;
		y+=dy;
		z+=dz;
	}
	
	//overload 1, hanya geser pada sumbu x
	public void geser(int dx){
		x+=dx;
	}
	
	//overload 2, hanya geser pada sumbu x dan y
	public void geser(int dx,int dy){
		x+=dx;
		y+=dy;
	}
	
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
	public int getZ(){
		return z;
	}
}
